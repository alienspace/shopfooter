import React, { Component } from 'react';
import {
  Grid, Col, Row,
  Image,
} from 'react-bootstrap';
import { APP_CONFIG } from './../../../boot.config';
import { styles } from './Footer.style';

/** CUSTOM COMPONENTS **/
// import { Logo } from './../../Logo/Logo';
export class Footer extends Component{
  render(){
    let year = new Date();
    year = year.getFullYear();
    return (
      <Col xs={12} md={12} className={'footerContainer'}>
        <Grid className={'no-padding'}>
          <Col xs={9} md={9} className={'no-padding'}>
            <Col xs={12} md={3} className={'no-padding'}>
              <h3>Perguntas Frequentes</h3>
            </Col>
            <Col xs={12} md={4}>
              <h3>Central de Relacionamento</h3>
            </Col>
            <Col xs={12} md={2}>
              <h3>Newsletter</h3>
            </Col>
            <Col xs={12} md={3}>
              <h3>Formas de Pagamento</h3>
              {/*<Image src={'http://sharpcpas.net/files/credit-cards.png'} responsive />*/}
              <Image src={'http://imgcentauro-a.akamaihd.net/content/images/pagamentoCredito_column.png'} responsive />
            </Col>
          </Col>
          <Col xs={3} md={3} className={'boxSocialContainer'}>
            <Col xs={12} md={12}>
              <h3>Facebook</h3>
            </Col>
            <Col xs={12} md={12}>
              <h3>Instagram</h3>
            </Col>
          </Col>
        </Grid>
        <Col xs={12} md={12} className={'copyright'}>
          <span> {APP_CONFIG.PROJECT_NAME} &copy; {year} - Todos os direitos reservados.</span>
        </Col>
        {/*
        <Col xs={12} md={2}>
          <a className={'assinatura'} title={'Desenvolvido por iDrops'} href={'http://www.dropspublicidade.com.br'} target={'_blank'} />
        </Col> */}
      </Col>
    );
  }
}
